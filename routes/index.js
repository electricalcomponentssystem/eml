var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
    views: importRoutes('./views'),
    news: importRoutes('./views/news'),
    services: importRoutes('./views/services'),

};
// Setup Route Bindings
exports = module.exports = function(app) {
    // Index
    app.all('/', routes.views.index);
    // Subscription
    app.all('/tilaa', keystone.middleware.api, routes.views.subscription);
    // Contact Us
    app.all('/ota-yhteytta', routes.views.contact);
    // About Us
    app.get('/tietoa-meista', routes.views.about);
    // News
    app.get('/uutiset', routes.news.index);
    // Get new by article
    app.get('/uutiset/artikkeli/:new', routes.news.singleNew);
    // Printing Services
    //app.get('/painopalvelut', routes.views.services.index);
    app.get('/painopalvelut/:services', routes.views.services.services);
    // Terms Of Serice
    app.get('/:title', routes.views.terms.index);
};