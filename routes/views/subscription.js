var keystone = require('keystone'),
    Subscript = keystone.list('Subscription');
var mongoose = require('mongoose');


exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.title = 'TWP - Subscribe';
    //locals.section = 'subscribe';
    locals.formData = req.body || {};
    locals.section = 'contact';
    locals.formData = req.body || {};
    locals.validationErrors = {};
    locals.emailFound = {};
    locals.success = {};
    locals.newsletterSubmitted = false;


    // On POST requests, add the Enquiry item to the database
    view.on('post', function(next) {
        var newSub = new Subscript.model({
            email: locals.formData.email
        });

        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }


        mongoose.model('Subscription').findOne({ 'email': locals.formData.email }, function(error, exist) {
            // Check if empty object received
            if (!Object.keys(locals.formData.email).length) {
                return res.apiError('Empty Form');
                next(error);
            } else if (validateEmail(locals.formData.email) !== true) {
                return res.apiError('Try again');
                next(error);
            } else if (exist && !error) {
                return res.apiError('Email found already.');
                next(error);
            } else {
                newSub
                    .getUpdateHandler(req)
                    .process(req.body, { flashErrors: true }, function(err) {
                        if (err) {
                            locals.validationErrors = err.errors;
                            next();
                        } else {
                            locals.success = "Email added to list.";
                            locals.newsletterSubmitted = true;
                            next(err);
                        }
                    });
            }
        })

    });
    // Render the view
    view.render(function(err) {
        if (err) return res.apiError('error', err);
        res.apiResponse({
            error: locals.validationErrors,
            success: locals.success
        });
    });

};