var keystone = require('keystone');
var _ = require('lodash');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.title = "";

    locals.srvs = {};
    locals.contact = {};
    locals.news = {
        footer: {}
    };
    locals.services = {};
    locals.terms = {};

    // Load footer term links
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next(err);
            })
    });


    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.services = data;
                    next(err);
                }
            })
    })


    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort('-date').limit(4)
            .exec(function(err, data) {

                if (!_.isEmpty({ data })) {
                    locals.news.footer = data;
                    next(err);
                } else {
                    next(err);
                }
            })
    });

    function lowerCaseFirst(string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    }

    function isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }

        return true;
    }

    // To be able to access painopalvelut
    // First letter of query must be capitalized
    view.on('init', function(next) {
        var q = keystone.list('Service').model.findOne({ slug: lowerCaseFirst(req.params.services) });
        q.exec(function(err, results) {
            if (isEmpty(results) == false) {
                locals.title = "EML - " + results.title;
                locals.srvs = results;
                next(err);
            } else {
                return res.status(404).redirect('/404');
            }
        })
    });




    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next(err);
        })
    });

    // Render the view
    view.render('services/service');

};