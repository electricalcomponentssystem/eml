var keystone = require('keystone');
var _ = require('lodash');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;


    locals.contact = {};
    locals.data = {
        services: {}
    }
    locals.news = {
        footer: {}
    };
    locals.terms = {};

    // Load footer term links
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next(err);
            })
    });


    view.on('init', function(next) {
        keystone.list('Page').model.findOne()
            .where('page', 'Palvelut')
            .where('active', true)
            .populate('meta')
            .exec(function(err, page) {
                if (err) {
                    return next(err);
                } else {
                    locals.page = page;
                    console.log(page);
                    next(err);
                }
            });
    });


    // Services
    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.data.services = data;
                    next(err);
                }
            });
    });


    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort('-date').limit(4)
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    if (!_.isEmpty({ data })) {
                        locals.news.footer = data;
                        next(err);
                    } else {
                        next(err);
                    }
                }
            })
    });


    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next(err);
        })
    });



    // Render the view
    view.render('services/index');

};