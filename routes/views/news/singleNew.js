var keystone = require('keystone');
var _ = require('lodash');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    var tt = "";

    // Set locals
    locals.section = 'new';
    locals.filters = {
        new: req.params.new,
    };
    locals.data = {
        news: [],
    };
    locals.contact = {};
    locals.news = {
        footer: {}
    }
    locals.services = {};
    locals.terms = {};

    // Load footer term links
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next(err);
            })
    });

    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.services = data;
                    next(err);
                }
            })
    })


    //Load 4 recent news to footer
    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort('-date').limit(4)
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    if (!_.isEmpty({ data })) {
                        locals.news.footer = data;
                        next(err);
                    }
                }
            })
    });


    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next(err);
        })
    });

    // Load the current post
    view.on('init', function(next) {
        var q = keystone.list('News').model.findOne({
            state: 'published',
            slug: locals.filters.new,
        }).populate('author categories');
        q.exec(function(err, result) {
            if (!result) {
                return res.redirect('/uutiset');
            }
            locals.title = "ELM - " + req.params.new;
            locals.data.news = result;
            return next(err);
        });

    });


    // Render the view
    view.render('news/new');
};