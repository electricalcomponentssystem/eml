var keystone = require('keystone');
var _ = require('lodash');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // locals.section is used to set the currently selected
    // item in the header navigation.
    locals.title = 'TWP - Uutiset'
    locals.section = 'news';
    locals.recent = {};
    locals.page = {};
    locals.services = {};
    locals.data = {
        news: {},
        categories: {},
    };
    locals.contact = {};
    locals.news = {
        footer: {}
    }
    locals.terms = {};

    // Load footer term links
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next(err);
            })
    });

    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.services = data;
                    next(err);
                }
            })
    })

    //Load 4 recent news to footer
    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort('-date').limit(4)
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    if (!_.isEmpty({ data })) {
                        locals.news.footer = data;
                        next(err);
                    }
                }
            })
    });

    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next(err);
        })
    });

    view.on('init', function(next) {
        keystone.list('Page').model.findOne()
            .where('page', 'Uutiset')
            .where('active', true)
            .populate('meta')
            .exec(function(err, page) {
                if (err) {
                    return next(err);
                } else {
                    locals.page = page;
                    next(err);
                }
            });
    });

    // Load the news
    view.on('init', function(next) {
        var q = keystone.list('News').paginate({
                page: req.query.page || 1,
                perPage: 3,
                maxPages: 10,
                filters: {
                    state: 'published',
                },
            })
            .sort('-publishedDate')
            .populate('author ');

        if (locals.data.category) {
            q.where('categories').in([locals.data.category]);
        }

        q.exec(function(err, results) {
            locals.data.news = results;
            next(err);
        });
    });


    view.on('init', function(next) {
        var q = keystone.list('News').model.find().sort({ 'publishedDate': -1 })
            .where('state', 'published')
            .limit(1)
            .exec(function(err, newest) {
                if (err) {
                    return next(err);
                } else {
                    locals.recent = newest;
                    next(err);
                }
            })
    });



    // Render the view
    view.render('./news/news');
};