var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    var title = 'TWP - ' + title;

    locals.page = {};
    locals.contact = {};
    locals.services = {};
    locals.terms = {};
    locals.cTerm = {};
    locals.filters = {
        new: req.params.new,
    };

    // Load curren term
    view.on('init', function(next) {
        var q = keystone.list('Term').model.findOne()
            .where('url', req.params.title)
            //.populate('author');

        q.exec(function(err, result) {

            if (!result) {
                return res.redirect('/');
            }
            locals.cTerm = result;
            locals.title = `TWP - ${result.title}`
            next(err);
        });

    });

    // Load all Terms
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next(err);
            })
    });


    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.services = data;
                    next(err);
                }
            })
    })


    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next(err);
        })
    });



    view.on('init', function(next) {
        keystone.list('Page').model.findOne()
            .where('page', 'Terms')
            .where('active', true)
            .exec(function(err, page) {
                if (err) {
                    return next(err);
                } else {
                    locals.page = page;
                    next(err);
                }
            });
    });


    // Render the view
    view.render('terms/index');

};