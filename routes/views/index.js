var keystone = require('keystone');
var _ = require('lodash');
// Just a TT test
var tt = require('../loader/news');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // locals.section is used to set the currently selected
    // item in the header navigation.
    locals.title = 'TWP - Etusivu'
    locals.section = 'home';
    locals.latestNews = {};
    locals.formData = req.body || {};
    locals.page = {};
    locals.contact = {};
    locals.favicon = {};
    locals.services = {};
    locals.carousels = {};
    locals.news = {
        footer: {}
    }
    locals.social = {};
    locals.validationErrors = {};
    locals.newsletterSubmitted = false;
    locals.terms = {};

    view.on('init', function(next) {
        keystone.list('Social').model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.social = data;
                next();
            })
    })

    // Load footer term links
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next();
            })
    });


    view.on('init', function(next) {
        var q = keystone.list('Carousel').model.find();
        q.exec(function(err, results) {
            if (err) {
                return next(err)
            }
            // Check starting date
            locals.carousels = results;
            next();
        })
    })

    //Load 4 recent news to footer
    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort('-date').limit(4)
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    if (!_.isEmpty({ data })) {
                        locals.news.footer = data;
                        next();
                    }
                }
            })
    });


    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next();
        });
    });


    // Load 3 recent news
    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort({ 'publishedDate': -1 }).limit(3)
            .exec(function(err, data) {
                if (err) {
                    return next(err)
                } else {
                    locals.latestNews = data;
                    next();
                }
            });
    });

    // Services
    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.services = data;
                    next();
                }
            })
    })


    view.on('init', function(next) {
        keystone.list('Page').model.findOne()
            .where('page', 'Etusivu')
            .where('active', true)
            .populate('meta')
            .exec(function(err, page) {
                if (err) {
                    return next(err);
                } else {
                    locals.page = page;
                    next();
                }
            });
    });
    view.render('index');
};