var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');
var _ = require('lodash');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.title = 'TWP - About Us';
    locals.section = 'about';

    locals.enquiryTypes = Enquiry.fields.enquiryType.ops;
    locals.formData = req.body || {};
    locals.validationErrors = {};
    locals.enquirySubmitted = false;
    locals.data = {};
    locals.about = {};
    locals.brochure = {};
    locals.employe = {};
    locals.page = {};
    locals.services = {};
    locals.projects = {};
    locals.contact = {};
    locals.news = {
        footer: {}
    };
    locals.terms = {};

    // Load footer term links
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next(err);
            });
    });

    view.on('init', function(next) {
        keystone.list("Project").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.projects = data;
                next(err);
            });
    });

    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.services = data;
                    next(err);
                }
            });
    });


    view.on('init', function(next) {
        keystone.list("Employee").model.find({})
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.employe = data;
                next(err);
            })
    });


    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort('-date').limit(4)
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.news.footer = data;
                    next(err);
                }
            });
    });


    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next(err);
        });
    });



    view.on('init', function(next) {
        keystone.list('Page').model.findOne()
            .where('page', 'about')
            .where('active', true)
            .exec(function(err, page) {
                if (err) {
                    return next(err);
                } else {
                    locals.about = page;
                    next(err);
                }
            });
    });

    view.render('about');
};