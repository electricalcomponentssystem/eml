var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');
var _ = require('lodash');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // Set locals
    locals.title = 'TWP - Ota Yhteyttä';

    locals.section = 'contact';
    locals.enquiryTypes = Enquiry.fields.enquiryType.ops;
    locals.formData = req.body || {};
    locals.validationErrors = {};
    locals.enquirySubmitted = false;
    locals.data = {};
    locals.services = {};
    locals.page = {};
    locals.contact = {};
    locals.socials = {};
    locals.news = {
        footer: {}
    };
    locals.terms = {};

    // Load footer term links
    view.on('init', function(next) {
        keystone.list("Term").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                }
                locals.terms = data;
                next(err);
            })
    });


    view.on('init', function(next) {
        keystone.list("Service").model.find()
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    locals.services = data;
                    next(err);
                }
            })
    })

    view.on('init', function(next) {
        keystone.list("Social").model.find({})
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    if (!_.isEmpty({ data })) {
                        locals.socials = data;
                        next(err);
                    }
                }
            });
    });


    view.on('init', function(next) {
        keystone.list("News").model.find({ "state": "published" }).sort('-date').limit(4)
            .exec(function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    if (!_.isEmpty({ data })) {
                        locals.news.footer = data;
                        next(err);
                    }
                }
            })
    });


    view.on('init', function(next) {
        var q = keystone.list('Contact').model.find();
        q.exec(function(err, results) {
            locals.contact = results;
            next(err);
        })
    })


    view.on('init', function(next) {
        keystone.list('Page').model.findOne()
            .where('page', 'Yhteystiedot')
            .where('active', true)
            .populate('meta')
            .exec(function(err, page) {
                if (err) {
                    return next(err);
                } else {
                    locals.page = page;
                    next();
                }
            });
    });


    view.on('post', { action: 'contact' }, function(next) {
        var newEnquiry = new Enquiry.model();
        var updater = newEnquiry.getUpdateHandler(req);
        updater.process(req.body, {
            flashErrors: true,
            fields: 'name, email, phone, enquiryType, message',
            errorMessage: 'There was a problem submitting your enquiry:',
        }, function(err) {
            if (err) {
                locals.validationErrors = err.errors;
            } else {
                locals.enquirySubmitted = true;
            }
            next();
        });
    });

    view.render('contactus');
};