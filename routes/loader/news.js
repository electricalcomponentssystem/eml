var mongoose = require('mongoose');
var News = mongoose.model('News', News);


module.exports = function() {
    News.find({}, function(err, result) {
        if (err) {
            return next(err);
        }
        og(result);
        return result;
    });
};