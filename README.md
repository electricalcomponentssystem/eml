# README #


**What’s a change log?**

A change log is a file which contains a curated, chronologically ordered list of notable changes for each version of a project.
[http://keepachangelog.com/en/0.3.0/](Why should I care?)

Versionumerointia ei vielä kehitys vaiheessa ole järkevää määritellä.
Versioinnissa käytetään semanttista versiointia, jota tulen avaamaan myöhäisemmässä vaiheessa.
Tästä syystä kehityksen alla olevan projektion versiointiin tulen merkkaamaan 0.0.0

# Change Log
All notable changes to this project will be documented in this file.


## [0.0.0] - 27.02.2017
### Changed
- Yhteydenottolomakkeen toimivuutta parannettu mm. ajax, formin lähetys tyhjänä estetty.
## [0.0.0] - 23.02.2017
### Changed
- Jumbotronin rgba(0,0,0, 0.7) kuvan päällä oleva filtteri poistettu.

## [0.0.0] - 00.00.0000
### Added
- Changelog lisätty README :hin.