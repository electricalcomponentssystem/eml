'use strict';

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    nodemon = require('gulp-nodemon'),
    sass = require('gulp-sass'),
    livereload = require('gulp-livereload'),
    jshint = require('gulp-jshint'),
    gutil = require('gulp-util');


var paths = {
    scripts: ['public/js/script.js'],
    sass: ['public/styles/sass/**/*.scss'],
};

gulp.task('server', function() {
    nodemon({ script: 'keystone.js' });
});

gulp.task('lint', function() {
    return gulp.src(paths.scripts)
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
});

gulp.task('scripts', function() {
    return gulp.src(paths.scripts)
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js/'))
});

gulp.task('sass', function() {
    return gulp.src(paths.sass)
        .pipe(sass().on('error', sass.logError))
        .pipe(livereload())
        .on('error', function(err) {
            console.error(err.message);
        })
        .pipe(concat('main.min.css'))
        .pipe(gulp.dest('public/styles'))
});

// gulp.task('jade', function() {
// 	return gulp.src(paths.jade)
// 		.pipe(jade({
// 			client: true
//     })).on('error', function(err) {
//       console.error(err.message)
//     })
// 		.pipe(livereload())
// });

gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.scripts, ['lint', 'scripts']);
});

gulp.task('serve', ['server', 'sass', 'scripts', 'lint', 'watch']);