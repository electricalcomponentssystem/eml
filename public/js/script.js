 $(document).ready(function() {
     var timer;
     var url = window.location;
     var form = $('#enq');
     var newsletterForm = $('#newsletter');
     var err, sent;
     var formMessages = $('.enquiry-message');
     var newsletterMessages = $('.newsletter-message');
     var loc = window.location.pathname;

     $(function() {
         var pathname = window.location.pathname;
         if (pathname == "/") {
             $('.jumbotron').height(572);
         } else {
             $('.jumbotron').height(480);
         }

     });

     function init_carousel() {
         H = +($(window).height() /* -height here  */ ); // or $('.carousel-inner') as you want ...
         $('.carousel-inner').css('height', H + 'px');
     }
     window.onload = init_carousel;
     init_carousel();

     $("ul.footer-news > li > a").hover(function() {
         $(this).css({});
     }, function() {
         $(this).removeAttr("style");
     });

     $('ul.nav a[href="' + url + '"]').parent().addClass('active');
     $('ul.nav a').filter(function() {
         return this.href == url;
     }).parent().addClass('active');


     $('#pills').find('a').each(function() {
         if ($(this).attr('href') === loc.substr(1)) {
             $(this).addClass('active');
         }
     });

     $('.navbar').append('<span class="nav-bg"></span>');
     $('.dropdown-toggle').click(function() {
         if (!$(this).parent().hasClass('open')) {
             $('.dropdown-menu  li').fadeIn(5000);
             $('html').addClass('menu-open');
         } else {
             $('html').removeClass('menu-open');
         }
     });

     $('.dropdown-toggle').blur(function() {
         $('html').removeClass('menu-open');
     });

     $('.service-icon img').hover(function() {
         $(this).addClass('animated bounce');
     }, function() {
         timer = setTimeout(function() { $('.service-icon img').removeClass('animated bounce'); }, 1000);
     });
     $('.carousel').carousel({
         interval: 2000
     });

     $("ol.carousel-indicators  li:first").addClass("active");
     $('ol.carousel-indicators  li').on("click", function() {
         $('ol.carousel-indicators li.active').removeClass("active");
         $(this).addClass("active");
     });

     // Enable Carousel Controls
     $(".left").click(function() {
         $("#myCarousel").carousel("prev");
     });
     $(".right").click(function() {
         $("#myCarousel").carousel("next");
     });


     //  $('#submit-enquery').addClass('error').attr("disabled", true);
     //  var form = $('#newsletter-submit');
     //  if (form.val().length < 5) {
     //      $('#submit-enquery').removeClass('error').attr("disabled", false);
     //  }

     $(".item:first-child").addClass("active");
     $('#enq input').each(function() {
         $('#submit-enquery').addClass('error').attr("disabled", true);

         $(this).blur(function() {
             var cplt = true;
             $('#enq input').each(function() {
                 if (!$(this).val()) {
                     cplt = false;
                 }
             });
             if (cplt === true) {
                 $('#submit-enquery').removeClass('error').attr("disabled", false);
             }
         });
     });

     // Newsletter
     // Disabled by default
     //$('#newsletter-submit').attr("disabled", true);
     //$(newsletterMessages).hide();
     var infoBox = $('.newsletter-form .info').hide();
     var errorBox = $('.newsletter-form .error').hide();
     $('#newsltr').submit(function(e) {
         e.preventDefault();
         var formData = $(this).serialize();
         $.ajax({
             type: 'POST',
             url: '/tilaa',
             data: formData
         }).done(function(response) {
             //$(newsletterForm).hide();
             //$(newsletterMessages).show();
             $('#newsltr input[name=email]').val('');
         }).fail(function(data) {
             err = true;
             if (data.responseText !== '') {
                 $(errorBox).append("<p>" + data.responseText.replace(/\{|\}/gi, '') + "</p>").fadeIn();
             } else {
                 $(errorBox).append("<p>" + data.responseText + "</p>").fadeIn();
             }
         }).success(function(data) {
             if (err === true) {
                 $(errorBox).hide();
             }
             $(infoBox).append("<p>" + data.success + "</p>").fadeIn().addClass('animated bounce');
         });
     });

     // Enquiry
     $('.enquiry-submitted').hide();
     $('#enq').submit(function(event) {
         event.preventDefault();
         var formData = $(this).serialize();
         $.ajax({
             type: 'POST',
             url: '/contact',
             data: formData
         }).done(function(response) {
             $(form).hide();
             $(formMessages).show();

             $('input[name]').val('');
             $('input[email]').val('');
             $('select').val('');
             $('textarea[message]').val('');

         }).fail(function(data) {
             $(formMessages).addClass('error');

             if (data.responseText !== '') {
                 $(formMessages).text(data.responseText);
             } else {
                 $(formMessages).text('OOps! An error occured and roud message could not be send.');
             }
         }).success(function(data) {
             var result = $('<div />').append(data).find('#enquiry-message').html();
             $('.enquiry-message').html(result);
         });
     });

     $('.services-img img').hover(function() {
         clearTimeout(timer);
         $(this).addClass('animated swing');
     }, function() {
         timer = setTimeout(function() { $('.services-img img').removeClass('animated swing'); }, 1000);
     });

     $("#service-listing .service-content").text(function(index, currentText) {
         return currentText.substr(0, 175) + '....';
     });

     $("article .new-listing-brief").text(function(index, currentText) {
         return currentText.substr(0, 175) + '....';
     });
     $('#about:empty').hide();
 });