var keystone = require('keystone');
var Types = keystone.Field.Types;



var Contact = new keystone.List('Contact', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true },
    note: 'Company information'
});


Contact.add({
    title: { type: String, required: true },
    section: {
        title: { type: Types.Text, note: 'This will be the title of this section in the footer.' },
    },
    favicon: { type: Types.CloudinaryImage },
    companyName: { type: Types.Text },
    companyFooter: { type: Types.Text },
    logo: { type: Types.CloudinaryImage },
    email: { type: Types.Email },
    phonenumber: { type: String },
    address: { type: String },
    postnumber: { type: String },
    cityOrRegion: { type: String },
    //employee: { type: Types.Relationship, ref: 'Employee', many: true },
});

// Contact.schema.pre('save', function(next) {
//     keystone.list('Contact').findOne({})
//         .exec(function(err, result) {
//             console.log(result);
//             if (Object.keys(result).length) {
//                 return next(new Error("Erorr, duplicate model."));
//             }
//             next();
//         });
// });


Contact.defaultColumns = 'title';
Contact.register();