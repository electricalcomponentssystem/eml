var keystone = require('keystone');
var Types = keystone.Field.Types;


var Contact = new keystone.List('Contact', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true },
    note: 'Contact Information'
});


Contact.add({
    title: { type: String, required: true },
    logo: { type: Types.CloudinaryImage },
    heroImage: { types: Types.CloudinaryImage },
    email: { type: Types.Email },
    phonenumber: { type: Types.Number },
    location: {
        address: { type: String },
        postnumber: { type: Types.Number },
        cityOrRegion: { type: String },
    },
    socials: { type: Types.Relationship, ref: 'Social', many: true }
    //employee: { type: Types.Relationship, ref: 'Employee', many: true },
})


Contact.defaultColumns = 'title';