var keystone = require('keystone');
var Types = keystone.Field.Types;


var Employee = new keystone.List('Employee', {
    autokey: { from: 'name', path: 'key', unique: true },
});

Employee.add({
    name: { type: String, required: true },
    creator: { type: Types.Relationship, ref: 'User', index: true },
    occupation: { type: Types.Text },
    email: { type: Types.Email, default: 'info@elm.fi', required: true },
    phonenumber: { type: Types.Text, format: false },
    image: { type: Types.CloudinaryImage },
    medias: {
        facebook: { type: Types.Text },
        twitter: { type: Types.Text },
        instagram: { type: Types.Text },
        linkedIn: { type: Types.Text },

    }
});


Employee.defaultColumns = 'name, email, phonenumber';
Employee.register();
//Employee.relationship({ ref: 'Contact', path: 'employees' });