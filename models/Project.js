var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Post Model
 * ==========
 */

var Project = new keystone.List('Project', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true },
});

Project.add({
    title: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
    author: { type: Types.Relationship, ref: 'User', index: true, Default: 'User' },
    publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } },
    image: { type: Types.CloudinaryImage },
    content: { type: Types.Html, wysiwyg: true, height: 400 },
});

// Post.schema.virtual('content.full').get(function() {
//     return this.content.extended || this.content.brief;
// });

Project.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%';
Project.register();