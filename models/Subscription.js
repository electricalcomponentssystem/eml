var keystone = require('keystone');
var Types = keystone.Field.Types;


var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


var Subscription = new keystone.List('Subscription', {
    map: { name: 'email' },
});


Subscription.add({
    email: { type: Types.Email },
})


Subscription.register();