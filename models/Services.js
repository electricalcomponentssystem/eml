var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Service Model
 * ==========
 */
var Service = new keystone.List('Service', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true }

});

Service.add({
    title: { type: Types.Text, note: 'This wil be the title of current service.' },
    active: { type: Types.Boolean, note: 'If this is set to be active, current service will show up at frontpage at service section. The recommended number of services displayed at a time would be six.' },
    image: { type: Types.CloudinaryImage },
    brief: { type: Types.Html, wysiwyg: true, note: 'This will be the small text under the service image link. [2]' }
});


Service.register();