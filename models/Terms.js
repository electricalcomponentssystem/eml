var keystone = require('keystone');
var Types = keystone.Field.Types;
var mongoose = require('mongoose');


/**
 * Service Model
 * ==========
 */
var Term = new keystone.List('Term', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true },

});

Term.add({
    title: { type: Types.Text, note: 'This wil be the title of current term.' },
    url: { type: Types.Text, note: 'This will be the URL path of current term. Please note that scandic characters (å, ä, ö) and German umlaut (ö, ä, ü) in URL path are not properly displayed.' },
    span: { type: Types.Text, note: 'This will be the text under the title. Span works as part of design element.' },
    content: { type: Types.Html, wysiwyg: true, height: 150, note: 'TXT goes here' },
    section: {
        title: { type: Types.Text, note: 'This will be the title of Terms section on footer.' }
    }
});


Term.schema.pre('save', true, function(next, done) {
    var self = this;

    function replaceChars(str) {

        return str.replace(/[\ae\ä\ö\aeae]/g, 'a')
            .replace(/[\u00fc\u00fb\u00f9\u00fa]/g, 'u')
            .replace(' ', '-');
    }

    keystone.list('Term').model.findOne({ slug: this.slug }).exec(function(err, slug) {
        if (err) {
            return next(err);
        }

        console.log(self.niga);
        process.nextTick(done);
        next();
    });
});

Term.register();