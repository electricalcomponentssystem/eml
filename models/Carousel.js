var keystone = require('keystone');
var Types = keystone.Field.Types;


var Carousel = new keystone.List('Carousel', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true },
    note: 'Carousel to display campaigns & ads.'
});


Carousel.add({
    title: { type: String, required: true, note: 'This will be the title of current carousel.' },
    background: { type: Types.CloudinaryImage, note: 'The background image of carousel. ' },
    content: { type: Types.Html, wysiwyg: true, note: 'Content box.' }
    //startingDate: { type: Types.Date, default: Date.now(), format: ('DD-MM-YYYY') }
});


Carousel.defaultColumns = 'title';
Carousel.register();