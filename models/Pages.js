var keystone = require('keystone');
var Types = keystone.Field.Types;
var Service = new keystone.List('Service');

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

/**
 * Special Page Model
 * ==================
 */
var Pages = [
    'Etusivu',
    'Uutiset',
    'Palvelut',
    'about',
    'Yhteystiedot',
    'Terms',
    'All'
];


var Page = new keystone.List('Page', {
    map: { name: 'title' },
    plural: 'Pages'
});


Page.add({
    active: { type: Types.Boolean },
    title: { type: Types.Text, required: true, intial: true },
    page: { type: Types.Select, options: Pages, note: 'Choose which page this custom data is for.  Make sure there is only one Page per Page type Active.' },
    home: {
        service: {
            title: { type: Types.Text, dependsOn: { page: 'Etusivu' }, note: 'This will be the title of Our Serivces section at front page.' },
        },
        about: {
            title: { type: Types.Text, dependsOn: { page: 'Etusivu' }, note: 'This will be the title of About Us section at front page.' },
            background: { type: Types.CloudinaryImage, dependsOn: { page: 'Etusivu' }, note: 'This is the background image wich will shown on About Us sectiona at front page.' },
            leftColumn: { type: Types.Html, wysiwyg: true, dependsOn: { page: 'Etusivu' }, note: 'This is the left column of about us section at front page. Suggested amount of characters is 580 per column.' },
            rightColumn: { type: Types.Html, wysiwyg: true, dependsOn: { page: 'Etusivu' }, note: 'This is the right column of about us section at front page. Suggested amount of characters is 580 per column.' },
        },
        new: {
            title: { type: Types.Text, dependsOn: { page: 'Etusivu' } },
            smallTitle: { type: Types.Text, dependsOn: { page: 'Etusivu' } }
        }
    },
    news: {
        image: { type: Types.CloudinaryImage, dependsOn: { page: 'Uutiset' } }
    },
    services: {
        background: { type: Types.CloudinaryImage, dependsOn: { page: 'Palvelut' } },
    },
    about: {
        jumbotron: { type: Types.Html, wysiwyg: true, dependsOn: { page: 'about' }, note: 'Content of this text box will displayed at top of the About Us page.' },
        jumbotronBackground: { type: Types.CloudinaryImage, dependsOn: { page: 'about' } },
        title: { type: Types.Text, dependsOn: { page: 'about' }, note: 'This will be the title of about us section.' },
        smallTitle: { type: Types.Text, dependsOn: { page: 'about' }, note: 'This will be the small title under the title.' },
        briefAboutUs: { type: Types.Html, wysiwyg: true, note: '.', dependsOn: { page: 'about' } },

    },
    contact: {
        jumbotron: {
            text: { type: Types.Html, wysiwyg: true, dependsOn: { page: 'Yhteystiedot' } },
        },
        getintouchTitle: { type: Types.Text, note: 'This will be the title at the top of the page', dependsOn: { page: 'Yhteystiedot' } },
        getintouchText: { type: Types.Text, note: 'This will be the small text under the landing text at the top of the page', dependsOn: { page: 'Yhteystiedot' } },
        bannerText: { type: Types.Text, note: 'This text will appear overlaying the banner image.', dependsOn: { page: 'Yhteystiedot' } },
        image: { type: Types.CloudinaryImage, dependsOn: { page: 'Yhteystiedot' } },
        enquiry: {
            message: { type: Types.Html, wysiwyg: true, dependsOn: { page: 'Yhteystiedot' }, note: 'Message after succecfully sent a contact us form.' }
        },
    },
    tos: {
        title: { type: Types.Text, note: 'This will be thet title at the top of the page', dependsOn: { page: 'Terms' } },
        statement: { type: Types.Html, wysiwyg: true, note: 'This will be the Terms Of Service statement, which locates at /tos.', dependsOn: { page: 'Terms' } }
    },
    footer: {
        newsTitle: { type: Types.Text, note: '', dependsOn: { page: 'All' } },
        customerService: { type: Types.Text, note: '', dependsOn: { page: 'All' } },
        contactInformation: { type: Types.Text, note: '', dependsOn: { page: 'All' } },
    }
});



Page.defaultColumns = 'title, page, active';
Page.register();