// Just a model to define Companys Social Networks
var keystone = require('keystone');
var Types = keystone.Field.Types;


var Social = new keystone.List('Social', {
    map: { name: 'name' },
    autokey: { path: 'slug', from: 'url', unique: true },
});

Social.add({
    name: { type: Types.Select, options: 'LinkedIn, Instagram, Facebook, Twitter, Youtube', default: 'LinkedIn' },
    url: { type: Types.Url }
});

Social.relationship({ ref: 'Company', path: 'companys', refPath: 'Socials' });

Social.register();